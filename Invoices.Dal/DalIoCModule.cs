using Autofac;
using Invoices.Dal.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Invoices.Dal
{
    public class DalIoCModule<TContext> : Module where TContext : DbContext
    {
        protected override void Load(ContainerBuilder builder)
        {
            //////////////////Services//////////////////
            builder.RegisterType<Repository<TContext>>()
                .As<IRepository>()
                .InstancePerLifetimeScope();
        }
    }
}

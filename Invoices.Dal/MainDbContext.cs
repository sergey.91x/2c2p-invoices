using Invoices.Dal.Models;
using Microsoft.EntityFrameworkCore;

namespace Invoices.Dal
{
    public class MainDbContext : DbContext
    {
        public MainDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Transaction> Transactions { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Transaction>().HasIndex(t => t.CurrencyCode);
            modelBuilder.Entity<Transaction>().HasIndex(t => t.TransactionDateTime);
            modelBuilder.Entity<Transaction>().HasIndex(t => t.Status);
        }
    }
}

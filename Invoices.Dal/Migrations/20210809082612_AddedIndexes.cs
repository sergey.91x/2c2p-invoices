﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Invoices.Dal.Migrations
{
    public partial class AddedIndexes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateIndex(
                name: "ix_transactions_currency_code",
                table: "transactions",
                column: "currency_code");

            migrationBuilder.CreateIndex(
                name: "ix_transactions_status",
                table: "transactions",
                column: "status");

            migrationBuilder.CreateIndex(
                name: "ix_transactions_transaction_date_time",
                table: "transactions",
                column: "transaction_date_time");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "ix_transactions_currency_code",
                table: "transactions");

            migrationBuilder.DropIndex(
                name: "ix_transactions_status",
                table: "transactions");

            migrationBuilder.DropIndex(
                name: "ix_transactions_transaction_date_time",
                table: "transactions");
        }
    }
}

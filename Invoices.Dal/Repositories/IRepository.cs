using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Invoices.Dal.Models.Entities.Abstract;

namespace Invoices.Dal.Repositories
{
    public interface IRepository
    {
        Task<ICollection<TEntity>> Get<TEntity>(
            Expression<Func<TEntity, bool>> filter = null,
            bool includeDeleted = false,
            Func<IQueryable<TEntity>,
                IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = null)
            where TEntity : class, IBaseEntity;

        Task<TEntity> GetById<TEntity>(
            Guid id,
            bool includeDeleted = false,
            string includeProperties = null)
            where TEntity : class, IBaseEntity;

        Task DeleteById<TEntity>(Guid id, bool isHardDelete = false) where TEntity : class, IBaseEntity;

        void Delete<TEntity>(TEntity entity, bool isHardDelete = false) where TEntity : class, IBaseEntity;

        Task<TEntity> Create<TEntity>(TEntity entity) where TEntity : class, IBaseEntity;

        Task CreateMany<TEntity>(IEnumerable<TEntity> entities) where TEntity : class, IBaseEntity;

        void Update<TEntity>(TEntity entity) where TEntity : class, IBaseEntity;

        void Update<TEntity>(IEnumerable<TEntity> entities) where TEntity : class, IBaseEntity;

        Task Save();
    }
}

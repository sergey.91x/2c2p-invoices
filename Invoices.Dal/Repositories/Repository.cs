using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Invoices.Dal.Models.Entities.Abstract;
using Microsoft.EntityFrameworkCore;

namespace Invoices.Dal.Repositories
{
    public class Repository<TContext> : IRepository where TContext : DbContext
    {
        private readonly TContext _context;

        public Repository(TContext context)
        {
            _context = context;
        }

        public async Task<ICollection<TEntity>> Get<TEntity>(
            Expression<Func<TEntity, bool>> filter = null,
            bool includeDeleted = false,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = null) where TEntity : class, IBaseEntity
        {
            return await GetQueryable(filter, includeDeleted, orderBy, includeProperties).ToListAsync()
                .ConfigureAwait(false);
        }

        public async Task<TEntity> GetById<TEntity>(
            Guid id,
            bool includeDeleted = false,
            string includeProperties = null)
            where TEntity : class, IBaseEntity
        {
            Expression<Func<TEntity, bool>> filter = x => x.Id == id;

            return await GetQueryable(filter, includeDeleted, includeProperties: includeProperties)
                .FirstOrDefaultAsync().ConfigureAwait(false);
        }

        public async Task<TEntity> Create<TEntity>(TEntity entity) where TEntity : class, IBaseEntity
        {
            entity.CreatedTimestamp = DateTime.UtcNow;
            var result = await _context.AddAsync(entity);

            return result.Entity;
        }

        public async Task CreateMany<TEntity>(IEnumerable<TEntity> entities) where TEntity : class, IBaseEntity
        {
            foreach (var entity in entities) entity.CreatedTimestamp = DateTime.UtcNow;

            await _context.AddRangeAsync(entities);
        }

        public void Update<TEntity>(TEntity entity) where TEntity : class, IBaseEntity
        {
            _context.Update(entity);
        }

        public void Update<TEntity>(IEnumerable<TEntity> entities) where TEntity : class, IBaseEntity
        {
            _context.UpdateRange(entities);
        }

        public async Task DeleteById<TEntity>(Guid id, bool isHardDelete = false) where TEntity : class, IBaseEntity
        {
            var entity = await GetById<TEntity>(id);

            Delete(entity, isHardDelete);
        }

        public void Delete<TEntity>(TEntity entity, bool isHardDelete = false) where TEntity : class, IBaseEntity
        {
            if (isHardDelete)
            {
                _context.Remove(entity);
                return;
            }

            entity.IsDeleted = true;
            _context.Update(entity);
        }

        public async Task Save()
        {
            await _context.SaveChangesAsync();
        }

        protected virtual IQueryable<TEntity> GetQueryable<TEntity>(
            Expression<Func<TEntity, bool>> filter = null,
            bool includeDeleted = false,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = null,
            int? skip = null,
            int? take = null)
            where TEntity : class, IBaseEntity
        {
            return PrepareQueryable(filter, includeDeleted, orderBy, includeProperties, skip, take).AsNoTracking();
        }

        protected virtual IQueryable<TEntity> PrepareQueryable<TEntity>(
            Expression<Func<TEntity, bool>> filter = null,
            bool includeDeleted = false,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            string includeProperties = null,
            int? skip = null,
            int? take = null)
            where TEntity : class, IBaseEntity
        {
            includeProperties ??= string.Empty;
            IQueryable<TEntity> query = _context.Set<TEntity>();

            if (filter != null) query = query.Where(filter);

            query = !includeDeleted
                ? query.Where(x => !x.IsDeleted)
                : query.IgnoreQueryFilters();

            query = includeProperties.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries)
                .Aggregate(query, (current, includeProperty) => current.Include(includeProperty));

            if (orderBy != null) query = orderBy(query);

            if (skip.HasValue) query = query.Skip(skip.Value);

            if (take.HasValue) query = query.Take(take.Value);

            return query;
        }
    }
}

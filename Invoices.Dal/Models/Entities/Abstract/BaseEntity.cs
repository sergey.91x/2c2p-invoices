using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Invoices.Dal.Models.Entities.Abstract
{
    public abstract class BaseEntity : IBaseEntity
    {
        protected BaseEntity()
        {
            Id = Guid.NewGuid();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public Guid Id { get; set; }

        [DataType(DataType.DateTime)]
        public DateTime CreatedTimestamp { get; set; }

        public bool IsDeleted { get; set; }
    }
}

using System;

namespace Invoices.Dal.Models.Entities.Abstract
{
    public interface IBaseEntity
    {
        Guid Id { get; set; }

        DateTime CreatedTimestamp { get; set; }

        bool IsDeleted { get; set; }
    }
}

using System;
using System.ComponentModel.DataAnnotations;
using Invoices.Common.Enums;
using Invoices.Dal.Models.Entities.Abstract;

namespace Invoices.Dal.Models
{
    public class Transaction : BaseEntity
    {
        [StringLength(50)]
        public string TransactionNumber { get; set; }

        public decimal Amount { get; set; }

        [StringLength(3)]
        public string CurrencyCode { get; set; }

        public DateTime TransactionDateTime { get; set; }

        public StatusEnum Status { get; set; }
    }
}

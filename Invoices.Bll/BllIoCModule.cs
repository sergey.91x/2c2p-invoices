using Autofac;
using Invoices.Bll.Services.Abstract;
using Invoices.Bll.Services.Concrete;

namespace Invoices.Bll
{
    public class BllIoCModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            //////////////////Services//////////////////
            builder.RegisterType<InvoiceParseService>()
                .As<IInvoiceParseService>()
                .InstancePerLifetimeScope();

            builder.RegisterType<ValidationFileHelper>()
                .As<IValidationFileHelper>()
                .InstancePerLifetimeScope();

            builder.RegisterType<InvoiceService>()
                .As<IInvoiceService>()
                .InstancePerLifetimeScope();
        }
    }
}

using System.Linq;
using Invoices.Bll.Services.Abstract;
using Invoices.Common.Extensions;
using Invoices.Exceptions.Concrete.Exceptions;
using Microsoft.AspNetCore.Http;

namespace Invoices.Bll.Services.Concrete
{
    public class ValidationFileHelper : IValidationFileHelper
    {
        public bool ValidateUploadingFile(
            IFormFile uploadingFile,
            string[] acceptableExtensions,
            long maxSizeBytes,
            bool isNeededToThrowException = true)
        {
            var isExtensionAcceptable = acceptableExtensions.ToList().Contains(uploadingFile.GetExtension());
            if (!isExtensionAcceptable) return ReturnUnsuccessfulState(isNeededToThrowException);

            var size = uploadingFile.Length;
            var isSizeValid = size != 0 && size <= maxSizeBytes;
            if (!isSizeValid) return ReturnUnsuccessfulState(isNeededToThrowException);

            return true;
        }

        private bool ReturnUnsuccessfulState(bool isNeededToThrowException)
        {
            if (isNeededToThrowException) throw new FileUploadException("The extension is not supported or the file size is exceeded");

            return false;
        }
    }
}

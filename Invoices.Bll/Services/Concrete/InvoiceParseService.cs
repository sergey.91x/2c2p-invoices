using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Xml.Serialization;
using CsvHelper;
using CsvHelper.Configuration;
using Invoices.Bll.Models.Transactions;
using Invoices.Bll.Services.Abstract;
using Invoices.Common.Enums;
using Invoices.Common.Extensions;
using Invoices.Common.Models.Concrete.Transactions;
using Invoices.Exceptions.Concrete.Exceptions;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;

namespace Invoices.Bll.Services.Concrete
{
    public class InvoiceParseService : IInvoiceParseService
    {
        private static readonly Dictionary<string, StatusEnum> StatusEnums = new()
        {
            { "approved", StatusEnum.A },
            { "failed", StatusEnum.R },
            { "finished", StatusEnum.D },
            { "rejected", StatusEnum.R },
            { "done", StatusEnum.D }
        };

        private readonly ILogger<InvoiceParseService> _logger;

        public InvoiceParseService(ILogger<InvoiceParseService> logger)
        {
            _logger = logger;
        }

        public IEnumerable<TransactionModel> ParseFile(IFormFile formFile)
        {
            _logger.LogInformation(
                "Get file to parse. {FileName}  {Length}",
                formFile.FileName,
                formFile.Length);

            return formFile.GetExtension() switch
            {
                "csv" => ParseCsvFile(formFile),
                "xml" => ParseXmlFile(formFile),
                _ => throw new BadParametersException($"Extension '{formFile.GetExtension()}' is not supported")
            };
        }

        private IEnumerable<TransactionModel> ParseXmlFile(IFormFile formFile)
        {
            var serializer = new XmlSerializer(typeof(TransactionsXml));
            using var reader = new StreamReader(formFile.OpenReadStream());

            var transactions = (TransactionsXml)serializer.Deserialize(reader);
            if (transactions?.Transactions == null) throw new BadParametersException("The list of transactions cannot be empty");

            foreach (var transaction in transactions.Transactions)
            {
                if (transaction.PaymentDetails == null) throw new BadParametersException("The payment details cannot be empty");

                var result = new TransactionModel
                {
                    Amount = transaction.PaymentDetails.Amount,
                    CurrencyCode = transaction.PaymentDetails.CurrencyCode.ToUpperInvariant(),
                    TransactionDate = transaction.TransactionDate,
                    TransactionId = transaction.Id,
                    Status = StatusEnums.ContainsKey(transaction.Status.ToLowerInvariant()) ?
                        StatusEnums[transaction.Status.ToLowerInvariant()] : null
                };

                if (!ValidateTransaction(result, out var errorMessage)) throw new BadParametersException(errorMessage);

                yield return result;
            }

            _logger.LogInformation(
                "Xml file parsed. {FileName}  {Length}",
                formFile.FileName,
                formFile.Length);
        }

        private IEnumerable<TransactionModel> ParseCsvFile(IFormFile formFile)
        {
            using var reader = new StreamReader(formFile.OpenReadStream());
            using var csv = new CsvReader(
                reader,
                new CsvConfiguration(CultureInfo.InvariantCulture)
                {
                    HasHeaderRecord = false
                });

            csv.Context.RegisterClassMap<CsvMap>();
            var records = csv.GetRecords<TransactionCsvModel>();
            foreach (var record in records)
            {
                var result = new TransactionModel
                {
                    Amount = record.Amount,
                    CurrencyCode = record.CurrencyCode.ToUpperInvariant(),
                    TransactionDate = record.TransactionDate,
                    TransactionId = record.TransactionId,
                    Status = StatusEnums.ContainsKey(record.Status?.ToLowerInvariant() ?? string.Empty) ?
                        StatusEnums[record.Status?.ToLowerInvariant() ?? string.Empty] : null
                };

                if (!ValidateTransaction(result, out var errorMessage)) throw new BadParametersException(errorMessage);

                yield return result;
            }

            _logger.LogInformation(
                "Csv file parsed. {FileName}  {Length}",
                formFile.FileName,
                formFile.Length);
        }

        private bool ValidateTransaction(TransactionModel transaction, out string errorMessage)
        {
            errorMessage = string.Empty;
            if (transaction == null)
            {
                errorMessage = "Transaction cannot be null";
                return false;
            }

            if (string.IsNullOrEmpty(transaction.TransactionId))
            {
                errorMessage = "Transaction id cannot be empty";
                return false;
            }

            if (string.IsNullOrEmpty(transaction.CurrencyCode))
            {
                errorMessage = "Currency code cannot be empty";
                return false;
            }

            if (transaction.CurrencyCode.Length != 3)
            {
                errorMessage = "Currency code is wrong";
                return false;
            }

            if (transaction.TransactionId.Length > 50)
            {
                errorMessage = "Transaction id cannot be more than 50 characters";
                return false;
            }

            if (!transaction.Status.HasValue)
            {
                errorMessage = $"{transaction.Status} not supported";
                return false;
            }

            return true;
        }
    }
}

public sealed class CsvMap : ClassMap<TransactionCsvModel>
{
    public CsvMap()
    {
        var format = "dd/MM/yyyy hh:mm:ss";
        AutoMap(CultureInfo.InvariantCulture);
        Map(m => m.TransactionDate).TypeConverterOption.Format(format)
            .TypeConverterOption.CultureInfo(CultureInfo.InvariantCulture).Index(3);
    }
}

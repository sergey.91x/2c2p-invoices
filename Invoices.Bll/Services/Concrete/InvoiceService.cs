using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Invoices.Bll.Services.Abstract;
using Invoices.Common.Enums;
using Invoices.Common.Models.Concrete.Response;
using Invoices.Dal.Models;
using Invoices.Dal.Repositories;
using Microsoft.AspNetCore.Http;

namespace Invoices.Bll.Services.Concrete
{
    public class InvoiceService : IInvoiceService
    {
        private readonly IInvoiceParseService _invoiceParseService;
        private readonly IRepository _repository;

        public InvoiceService(IRepository repository, IInvoiceParseService invoiceParseService)
        {
            _repository = repository;
            _invoiceParseService = invoiceParseService;
        }

        public async Task<IEnumerable<Guid>> LoadInvoice(IFormFile file)
        {
            var transactions = _invoiceParseService.ParseFile(file);
            var transactionsDb = transactions.Select(
                t =>
                {
                    Debug.Assert(t.Status != null, "t.Status != null");
                    return new Transaction
                    {
                        Id = Guid.NewGuid(),
                        Amount = t.Amount,
                        Status = t.Status.Value,
                        CreatedTimestamp = DateTime.UtcNow,
                        CurrencyCode = t.CurrencyCode,
                        TransactionNumber = t.TransactionId,
                        TransactionDateTime = t.TransactionDate
                    };
                }).ToList();

            await _repository.CreateMany(transactionsDb);
            await _repository.Save();
            return transactionsDb.Select(t => t.Id);
        }

        public async Task<IEnumerable<TransactionResponseModel>> GetTransactions(string currencyCode)
        {
            var result = await _repository.Get<Transaction>(
                transaction => transaction.CurrencyCode == currencyCode);

            return result.Select(MapTransactionToResponse);
        }

        public async Task<IEnumerable<TransactionResponseModel>> GetTransactions(DateTime fromDate, DateTime? toDate = null)
        {
            var result = await _repository.Get<Transaction>(
                transaction => transaction.TransactionDateTime >= fromDate
                               && (!toDate.HasValue || transaction.TransactionDateTime <= toDate));

            return result.Select(MapTransactionToResponse);
        }

        public async Task<IEnumerable<TransactionResponseModel>> GetTransactions(StatusEnum statusEnum)
        {
            var result = await _repository.Get<Transaction>(
                transaction => transaction.Status == statusEnum);

            return result.Select(MapTransactionToResponse);
        }

        private TransactionResponseModel MapTransactionToResponse(Transaction transaction)
        {
            if (transaction == null) return null;
            return new TransactionResponseModel
            {
                Id = transaction.TransactionNumber,
                Payment = $"{transaction.Amount} {transaction.CurrencyCode}",
                Status = transaction.Status.ToString()
            };
        }
    }
}

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Invoices.Common.Enums;
using Invoices.Common.Models.Concrete.Response;
using Microsoft.AspNetCore.Http;

namespace Invoices.Bll.Services.Abstract
{
    public interface IInvoiceService
    {
        Task<IEnumerable<Guid>> LoadInvoice(IFormFile file);

        Task<IEnumerable<TransactionResponseModel>> GetTransactions(string currencyCode);

        Task<IEnumerable<TransactionResponseModel>> GetTransactions(DateTime fromDate, DateTime? toDate = null);

        Task<IEnumerable<TransactionResponseModel>> GetTransactions(StatusEnum status);
    }
}

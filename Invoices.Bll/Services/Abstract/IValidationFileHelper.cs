using Microsoft.AspNetCore.Http;

namespace Invoices.Bll.Services.Abstract
{
    public interface IValidationFileHelper
    {
        bool ValidateUploadingFile(
            IFormFile uploadingFile,
            string[] acceptableExtensions,
            long maxSizeMb,
            bool isNeededToThrowException = true);
    }
}

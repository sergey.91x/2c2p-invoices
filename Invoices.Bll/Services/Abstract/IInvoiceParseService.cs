using System.Collections.Generic;
using Invoices.Common.Models.Concrete.Transactions;
using Microsoft.AspNetCore.Http;

namespace Invoices.Bll.Services.Abstract
{
    public interface IInvoiceParseService
    {
        IEnumerable<TransactionModel> ParseFile(IFormFile formFile);
    }
}

using System;
using CsvHelper.Configuration.Attributes;

namespace Invoices.Bll.Models.Transactions
{
    public class TransactionCsvModel
    {
        [Index(0)]
        public string TransactionId { get; set; }

        [Index(1)]
        public decimal Amount { get; set; }

        [Index(2)]
        public string CurrencyCode { get; set; }

        public DateTime TransactionDate { get; set; }

        [Index(4)]
        public string Status { get; set; }
    }
}

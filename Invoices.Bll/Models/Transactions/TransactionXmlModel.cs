using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Invoices.Bll.Models.Transactions
{
    [XmlRoot(ElementName = "PaymentDetails")]
    public class PaymentDetailsXml
    {
        [XmlElement(ElementName = "Amount")]
        public decimal Amount { get; set; }

        [XmlElement(ElementName = "CurrencyCode")]
        public string CurrencyCode { get; set; }
    }

    [XmlRoot(ElementName = "Transaction")]
    public class TransactionXml
    {
        [XmlElement(ElementName = "TransactionDate")]
        public DateTime TransactionDate { get; set; }

        [XmlElement(ElementName = "PaymentDetails")]
        public PaymentDetailsXml PaymentDetails { get; set; }

        [XmlElement(ElementName = "Status")]
        public string Status { get; set; }

        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }

        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "Transactions")]
    public class TransactionsXml
    {
        [XmlElement(ElementName = "Transaction")]
        public List<TransactionXml> Transactions { get; set; }
    }
}

using Microsoft.AspNetCore.Http;

namespace Invoices.Common.Models.Abstract
{
    public abstract class UploadFileBaseRequestModel
    {
        public IFormFile File { get; set; }
    }
}

using Invoices.Common.Enums;

namespace Invoices.Common.Models.Concrete.Request
{
    /// <summary>
    ///     Model for get transactions by status
    /// </summary>
    public class GetTransactionsByStatusRequestModel
    {
        public StatusEnum Status { get; set; }
    }
}

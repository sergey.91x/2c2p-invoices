namespace Invoices.Common.Models.Concrete.Request
{
    /// <summary>
    ///     Model for get transactions by currency code
    /// </summary>
    public class GetTransactionsByCurrencyRequestModel
    {
        public string CurrencyCode { get; set; }
    }
}

using System;

namespace Invoices.Common.Models.Concrete.Request
{
    /// <summary>
    ///     Model for get transactions by date range
    /// </summary>
    public class GetTransactionsByDateRangeRequestModel
    {
        public DateTime FromDate { get; set; }

        public DateTime? ToDate { get; set; }
    }
}

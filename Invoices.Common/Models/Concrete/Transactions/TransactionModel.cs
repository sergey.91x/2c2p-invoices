using System;
using Invoices.Common.Enums;

namespace Invoices.Common.Models.Concrete.Transactions
{
    public class TransactionModel
    {
        public string TransactionId { get; set; }

        public decimal Amount { get; set; }

        public string CurrencyCode { get; set; }

        public DateTime TransactionDate { get; set; }

        public StatusEnum? Status { get; set; }
    }
}

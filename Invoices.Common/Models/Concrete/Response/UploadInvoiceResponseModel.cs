using System;
using System.Collections.Generic;

namespace Invoices.Common.Models.Concrete.Response
{
    public class UploadInvoiceResponseModel
    {
        public IEnumerable<Guid> TransactionIds { get; set; }
    }
}

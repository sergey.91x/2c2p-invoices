namespace Invoices.Common.Models.Concrete.Response
{
    public class TransactionResponseModel
    {
        public string Id { get; set; }

        public string Payment { get; set; }

        public string Status { get; set; }
    }
}

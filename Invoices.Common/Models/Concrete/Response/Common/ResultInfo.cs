﻿namespace Invoices.Common.Models.Concrete.Response.Common
{
    public class ResultInfo
    {
        public ResultInfo(string code = "success")
        {
            Code = code;
        }

        public string Code { get; set; }
    }
}

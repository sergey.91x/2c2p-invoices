namespace Invoices.Common.Enums
{
    public enum StatusEnum
    {
        /// <summary>
        ///     Approved
        /// </summary>
        A = 0,

        /// <summary>
        ///     Rejected
        /// </summary>
        R = 1,

        /// <summary>
        ///     Done
        /// </summary>
        D = 2
    }
}

using System.IO;
using Microsoft.AspNetCore.Http;

namespace Invoices.Common.Extensions
{
    public static class FormFileExtensions
    {
        public static string GetExtension(this IFormFile sourceFile)
        {
            return Path.GetExtension(sourceFile.FileName)?.Replace(".", "").ToLowerInvariant();
        }
    }
}

using System.Net;

namespace Invoices.Exceptions.Models
{
    public class ErrorInfo
    {
        public ErrorInfo()
        {
            Code = new CodeInfo();
        }

        public CodeInfo Code { get; set; }

        public ErrorDataInfo[] Errors { get; set; }
    }

    public class CodeInfo
    {
        public CodeInfo()
        {
            Code = new CodeDataInfo();
        }

        public string Description { get; set; }

        public string Field { get; set; }

        public CodeDataInfo Code { get; set; }

        public string StackTrace { get; set; }
    }

    public class CodeDataInfo
    {
        public string CodeString { get; set; }

        public HttpStatusCode HttpCode { get; set; }
    }

    public class ErrorDataInfo
    {
        public string Code { get; set; }

        public string Field { get; set; }

        public string Message { get; set; }

        public string StackTrace { get; set; }
    }
}

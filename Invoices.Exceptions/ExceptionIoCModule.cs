using Autofac;
using Invoices.Exceptions.Abstract;
using Invoices.Exceptions.Concrete;

namespace Invoices.Exceptions
{
    public class ExceptionIoCModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder.RegisterType<ErrorInfoBuilder>()
                .As<IErrorInfoBuilder>()
                .SingleInstance();
        }
    }
}

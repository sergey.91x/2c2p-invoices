using System.Net;

namespace Invoices.Exceptions.Abstract
{
    public interface ICustomException
    {
        string Code { get; }

        string MessageException { get; }

        HttpStatusCode HttpCode { get; }
    }
}

using System;
using Invoices.Exceptions.Models;

namespace Invoices.Exceptions.Abstract
{
    public interface IErrorInfoBuilder
    {
        ErrorInfo BuildErrorInfo(Exception exception);
    }
}

using System.IO;
using System.Threading.Tasks;
using Invoices.Exceptions.Abstract;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Invoices.Exceptions.Middleware
{
    public class JsonExceptionMiddleware
    {
        private readonly IErrorInfoBuilder _errorInfoBuilder;

        private readonly JsonSerializer _serializer;

        public JsonExceptionMiddleware(IErrorInfoBuilder errorInfoBuilder)
        {
            _errorInfoBuilder = errorInfoBuilder;

            _serializer = new JsonSerializer
            {
                ContractResolver = new CamelCasePropertyNamesContractResolver()
            };
        }

        public async Task Invoke(HttpContext context)
        {
            context.Response.ContentType = "application/json";

            var ex = context.Features.Get<IExceptionHandlerFeature>()?.Error;
            if (ex == null) return;

            var error = _errorInfoBuilder.BuildErrorInfo(ex);
            context.Response.StatusCode = (int)error.Code.Code.HttpCode;

            await using (var writer = new StreamWriter(context.Response.Body))
            {
                _serializer.Serialize(writer, error);
                await writer.FlushAsync().ConfigureAwait(false);
            }
        }
    }
}

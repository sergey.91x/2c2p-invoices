using System;
using System.Net;
using Invoices.Exceptions.Abstract;

namespace Invoices.Exceptions.Concrete.Exceptions
{
    public class BadParametersException : Exception, ICustomException
    {
        public BadParametersException(string messageException)
        {
            MessageException = messageException;
        }

        public string Code => "bad_request";

        public string MessageException { get; }

        public HttpStatusCode HttpCode => HttpStatusCode.BadRequest;
    }
}

using System;
using System.Net;
using Invoices.Exceptions.Abstract;

namespace Invoices.Exceptions.Concrete.Exceptions
{
    public class FileUploadException : Exception, ICustomException
    {
        public FileUploadException(string messageException)
        {
            MessageException = messageException;
        }

        public string Code => "file_upload_error";

        public string MessageException { get; }

        public HttpStatusCode HttpCode => HttpStatusCode.BadRequest;
    }
}

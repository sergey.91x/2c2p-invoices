using System;
using System.Net;
using Invoices.Exceptions.Abstract;
using Invoices.Exceptions.Models;
using Microsoft.Extensions.Logging;

namespace Invoices.Exceptions.Concrete
{
    public class ErrorInfoBuilder : IErrorInfoBuilder
    {
        private const string DefaultErrorMessage = "A server error occurred.";
        private readonly ILogger<ErrorInfoBuilder> _logger;

        public ErrorInfoBuilder(ILogger<ErrorInfoBuilder> logger)
        {
            _logger = logger;
        }

        public ErrorInfo BuildErrorInfo(Exception exception)
        {
            return exception switch
            {
                ICustomException customException => BuildCustomExceptionErrorInfo(customException),
                _ => BuildDefaultExceptionErrorInfo(exception)
            };
        }

        private ErrorInfo BuildCustomExceptionErrorInfo(ICustomException customException)
        {
            var error = new ErrorInfo();
            error.Code.Code.HttpCode = customException.HttpCode;
            error.Code.Code.CodeString = customException.Code;
            error.Code.Description = customException.MessageException;
            _logger.LogError(
                "{Message}",
                error.Code.Description);

            return error;
        }

        private ErrorInfo BuildDefaultExceptionErrorInfo(Exception exception)
        {
            var errorArray = new ErrorDataInfo();

            errorArray.Message = exception.Message;
            errorArray.Code = "internal_error";

            var error = new ErrorInfo();
            error.Code.Code.HttpCode = HttpStatusCode.InternalServerError;
            error.Code.Code.CodeString = "internal_error";
            error.Code.Description = DefaultErrorMessage;
            error.Errors = new[]
            {
                errorArray
            };

            _logger.LogError(
                "{Message}",
                error.Code.Description);

            return error;
        }
    }
}

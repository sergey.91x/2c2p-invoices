using System;
using System.Net;
using Invoices.Exceptions.Abstract;

namespace Invoices.Exceptions.Concrete
{
    public class CustomException : Exception, ICustomException
    {
        public CustomException(string code, string messageException, HttpStatusCode httpCode)
        {
            Code = code;
            MessageException = messageException;
            HttpCode = httpCode;
        }

        public string Code { get; }

        public string MessageException { get; }

        public HttpStatusCode HttpCode { get; }
    }
}

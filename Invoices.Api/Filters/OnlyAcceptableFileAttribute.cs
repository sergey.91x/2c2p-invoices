using System.Linq;
using Invoices.Bll.Services.Abstract;
using Invoices.Common.Models.Abstract;
using Invoices.Exceptions.Concrete.Exceptions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Invoices.Api.Filters
{
    /// <summary>
    ///     Filter for file types
    /// </summary>
    public sealed class OnlyAcceptableFileAttribute : TypeFilterAttribute
    {
        public OnlyAcceptableFileAttribute(string[] acceptableExtensions, long maxSize)
            : base(typeof(OnlyAcceptableFileFilter))
        {
            Arguments = new object[] { acceptableExtensions, maxSize };
        }

        private sealed class OnlyAcceptableFileFilter : ActionFilterAttribute
        {
            private readonly string[] _acceptableExtensions;
            private readonly long _maxSize;

            private readonly IValidationFileHelper _validationFileHelper;

            public OnlyAcceptableFileFilter(
                string[] acceptableExtensions,
                long maxSize,
                IValidationFileHelper validationFileHelper)
            {
                _maxSize = maxSize;
                _acceptableExtensions = acceptableExtensions;
                _validationFileHelper = validationFileHelper;
            }

            public override void OnActionExecuting(ActionExecutingContext context)
            {
                var uploadingFile = TryToRetrieveUploadingFile(context);
                _validationFileHelper.ValidateUploadingFile(uploadingFile, _acceptableExtensions, _maxSize);
            }

            private IFormFile TryToRetrieveUploadingFile(ActionExecutingContext context)
            {
                var requestModel = context?.ActionArguments?.Values?.FirstOrDefault();

                if (!(requestModel is UploadFileBaseRequestModel uploadingModel) || uploadingModel.File == null) throw new FileUploadException("Wrong request model or file empty");

                return uploadingModel.File;
            }
        }
    }
}

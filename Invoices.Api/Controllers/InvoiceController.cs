using System.Collections.Generic;
using System.Threading.Tasks;
using Invoices.Api.Filters;
using Invoices.Bll.Services.Abstract;
using Invoices.Common.Models.Concrete.Request;
using Invoices.Common.Models.Concrete.Response;
using Invoices.Common.Models.Concrete.Response.Common;
using Microsoft.AspNetCore.Mvc;

namespace Invoices.Api.Controllers
{
    /// <summary>
    ///     Controller for upload invoices and get transactions
    /// </summary>
    public class InvoiceController : ControllerBase
    {
        private readonly IInvoiceService _invoiceService;

        public InvoiceController(IInvoiceService invoiceService)
        {
            _invoiceService = invoiceService;
        }

        /// <summary>
        ///     Method for upload new invoice.
        /// </summary>
        /// <param name="request">File extension .csv or xml</param>
        /// <returns>Created transaction ids from db</returns>
        [HttpPost]
        [Route("/api/v1/file/invoice/upload")]
        [OnlyAcceptableFile(new[] { "csv", "xml" }, 1048576)]
        public async ValueTask<ResultInfo<UploadInvoiceResponseModel>> UploadInvoice(
            [FromForm] UploadFileRequestModel request)
        {
            var transactionsIds = await _invoiceService.LoadInvoice(request?.File);

            return new ResultInfo<UploadInvoiceResponseModel>
            {
                Data = new UploadInvoiceResponseModel
                {
                    TransactionIds = transactionsIds
                }
            };
        }

        /// <summary>
        ///     Get transactions by currency code
        /// </summary>
        /// <param name="request">For example USD</param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/v1/invoice/getTransactions/currencyCode")]
        public async ValueTask<ResultInfo<IEnumerable<TransactionResponseModel>>> GetTransactions(
            [FromQuery] GetTransactionsByCurrencyRequestModel request)
        {
            var result = await _invoiceService.GetTransactions(request?.CurrencyCode);

            return new ResultInfo<IEnumerable<TransactionResponseModel>>
            {
                Data = result
            };
        }

        /// <summary>
        ///     Get transactions by date range
        /// </summary>
        /// <param name="request">Date format for example 05/29/2015 05:50:06</param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/v1/invoice/getTransactions/dateRange")]
        public async ValueTask<ResultInfo<IEnumerable<TransactionResponseModel>>> GetTransactions(
            [FromQuery] GetTransactionsByDateRangeRequestModel request)
        {
            var result = await _invoiceService.GetTransactions(request.FromDate, request.ToDate);

            return new ResultInfo<IEnumerable<TransactionResponseModel>>
            {
                Data = result
            };
        }

        /// <summary>
        ///     Get transactions by status
        /// </summary>
        /// <param name="request">0-A, 1-R, D-2</param>
        /// <returns></returns>
        [HttpPost]
        [Route("/api/v1/invoice/getTransactions/status")]
        public async ValueTask<ResultInfo<IEnumerable<TransactionResponseModel>>> GetTransactions(
            [FromQuery] GetTransactionsByStatusRequestModel request)
        {
            var result = await _invoiceService.GetTransactions(request.Status);

            return new ResultInfo<IEnumerable<TransactionResponseModel>>
            {
                Data = result
            };
        }
    }
}

using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Reflection;
using Autofac;
using Autofac.Extensions.DependencyInjection;
using Invoices.Bll;
using Invoices.Dal;
using Invoices.Exceptions;
using Invoices.Exceptions.Abstract;
using Invoices.Exceptions.Middleware;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Serilog;
using Swashbuckle.AspNetCore.SwaggerUI;

namespace Invoices.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration, IWebHostEnvironment env)
        {
            Configuration = configuration;
            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(configuration, "Serilog")
                .Enrich.WithProperty("Project", env.ApplicationName)
                .Enrich.WithProperty("Env", env.EnvironmentName)
                .CreateLogger();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            //Logging
            services.AddLogging(loggingBuilder => loggingBuilder.AddSerilog(dispose: true));
            //Database
            var connection = Configuration.GetConnectionString("DefaultConnection");
            services.AddDbContext<MainDbContext>(
                options =>
                    options.UseNpgsql(connection).UseSnakeCaseNamingConvention()
            );

            //Swagger
            services.AddSwaggerGen(
                c =>
                {
                    var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                    var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                    if (File.Exists(xmlPath)) c.IncludeXmlComments(xmlPath);
                });

            //Newtonsoft Json
            services.AddControllers().AddNewtonsoftJson(
                options =>
                {
                    options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                    options.SerializerSettings.DefaultValueHandling = DefaultValueHandling.Include;
                    options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                });

            //Autofac
            var containerBuilder = new ContainerBuilder();
            containerBuilder.Populate(services);
            containerBuilder.RegisterModule(new BllIoCModule());
            containerBuilder.RegisterModule(new DalIoCModule<MainDbContext>());
            containerBuilder.RegisterModule(new ExceptionIoCModule());

            var container = containerBuilder.Build();
            var ioCServiceProvider = new AutofacServiceProvider(container);
            return ioCServiceProvider;
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IErrorInfoBuilder errorInfoBuilder, MainDbContext dataContext)
        {
            app.UseSerilogRequestLogging();
            //Auto migration
            dataContext.Database.Migrate();
            if (env.IsDevelopment()) app.UseDeveloperExceptionPage();
            else
                app.UseExceptionHandler(
                    appBuilder =>
                    {
                        appBuilder.Run(
                            async context =>
                            {
                                context.Response.StatusCode = 500;
                                await context.Response.WriteAsync("An unexpected error occured. Try again later.");
                            });
                    });

            app.UseExceptionHandler(
                new ExceptionHandlerOptions
                {
                    ExceptionHandler = new JsonExceptionMiddleware(errorInfoBuilder).Invoke
                });

            var cultureInfo = new CultureInfo("en-US");
            app.UseRequestLocalization(
                new RequestLocalizationOptions
                {
                    DefaultRequestCulture = new RequestCulture(cultureInfo),
                    SupportedCultures = new List<CultureInfo>
                    {
                        cultureInfo
                    },
                    SupportedUICultures = new List<CultureInfo>
                    {
                        cultureInfo
                    }
                });

            app.UseRouting();

            app.UseEndpoints(
                endpoints =>
                {
                    endpoints.MapDefaultControllerRoute();
                });

            var option = new RewriteOptions();
            option.AddRedirect("^$", "api/v1/swagger");
            app.UseRewriter(option);

            app.UseSwagger(c => { c.RouteTemplate = "api/v1/swagger/{documentName}/swagger.json"; });

            app.UseSwaggerUI(
                c =>
                {
                    c.SwaggerEndpoint("/api/v1/swagger/v1/swagger.json", "v1");
                    c.RoutePrefix = "api/v1/swagger";
                    c.DefaultModelExpandDepth(0);
                    c.DefaultModelsExpandDepth(-1);
                    c.DocExpansion(DocExpansion.None);
                });
        }
    }
}
